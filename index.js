const express = require("express");
const { default: mongoose } = require("mongoose");
const app = express();
const port = 8000;
const cards = require('./dbCards');
const cors = require("cors");
require("dotenv").config();
const clusterPassword = process.env.DB_CLUSTER_PASS;
const clusterUserName = process.env.DB_CLUSTER_USER;

//db connecting cluster URL
const mongo_db_URL= `mongodb+srv://${clusterUserName}:${clusterPassword}@cluster0.aqj1v9s.mongodb.net/?retryWrites=true&w=majority`;
//middlewares intializing before connecting to database
app.use(express.json());
app.use(cors());
//database connecting
mongoose.connect(mongo_db_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true
    }).then(()=>console.log("database connected successfully"))
    .catch((err)=>console.log("database is not connected"+err))
//get req handles
app.get('/',(req,res)=>{
    res.send('Welcome to the new world of time wasting tech');
})
//post req handles
app.post("/dating/cards",(req,res)=>{
    const data_from_user = req.body;
    cards.create(data_from_user,(err,data)=>{
        if(err){
            res.status(500).send(err);
        }else{
            res.status(201).send(data);
        }
    })
})
// get req handing on same path
app.get('/dating/cards',(req,res)=>{
    cards.find((err,data)=>{
        if(err){
            res.status(500).send(err);
        }else{
            res.status(200).send(data);
        }
    })
})




//listening server on port 
app.listen(process.env.port || port,()=>{
    console.log(`server is up on Port Number ${port}`);
})